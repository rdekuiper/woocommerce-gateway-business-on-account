<?php
/**
 * Woocommerce Gateway Business On Account
 *
 * Creates an on account payment method
 * Behaves the same as BACS, with the following alterations
 * 1. Order status is processing after the order is placed, so that the order can be shipped before payment
 * 2. Payment options is only available when the company name is filled in
 *
 * @link              https://bitbucket.org/rdekuiper/woocommerce-gateway-business-on-account/
 * @since             1.0.0
 * @package           Woocommerce_Gateway_Business_On_Account
 *
 * @wordpress-plugin
 * Plugin Name:       Woocommerce Gateway Business On Account
 * Plugin URI:        https://bitbucket.org/rdekuiper/woocommerce-gateway-business-on-account/
 * Description:       Creates an on account payment method that sets the order to processing after payment.
 * Version:           1.0.0
 * Author:            Robbert de Kuiper
 * Author URI:        https://www.robbertdekuiper.com/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       woocommerce-gateway-business-on-account
 * Domain Path:       /languages
 */
// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) die;

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-woocommerce-gateway-business-on-account.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_woocommerce_gateway_business_on_account() {
	if ( !class_exists( 'WooCommerce' ) ) {
		return new WP_Error( 'error', __( "Woocommerce not active, can't add payement method", "woocommerce-gateway-business-on-account" ) );
	}
	$plugin = new Woocommerce_Gateway_Business_On_Account();
	$plugin->run();
}
run_woocommerce_gateway_business_on_account();