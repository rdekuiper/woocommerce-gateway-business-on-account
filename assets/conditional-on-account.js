/**
 * Show on account payment method when billing company is filled in
 *
 * Trigger and update of the woocommerce checkout
 * when the billing company input field changes
 */
(function($) {
  // Find the company name input element
  var companyAddress = $("#billing_company");
  var invoiceNotice = $('.js-invoice-notice');

  if (companyAddress.length > 0) {
    // Find the value
    var companyAddressVal = companyAddress.val();
    toggleInvoiceNotice(companyAddressVal);
    // Listen for blur on that element
    companyAddress.on("blur", function() {
      // Only trigger on changes in the value
      if (companyAddressVal != companyAddress.val()){
        // Trigger WooCommerce checkout update
        $(document.body).trigger("update_checkout");
        // Update value for futher changes
        companyAddressVal = companyAddress.val();
        toggleInvoiceNotice(companyAddressVal);
      }
    });
  }

  function toggleInvoiceNotice(trigger) {
    if (trigger == '') {
      invoiceNotice.show();
      return;
    }
    invoiceNotice.hide();
  }

})(jQuery);