<?php
/**
 * The core plugin class.
 *
 *
 * @package    Woocommerce_Gateway_Business_On_Account
 * @subpackage Woocommerce_Gateway_Business_On_Account/includes
 * @author     Robbert de Kuiper <mail@robbertdekuiper.com>
*/

class Woocommerce_Gateway_Business_On_Account {

  /**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $plugin_name    The string used to uniquely identify this plugin.
	 */
	protected $plugin_name;

  /**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $plugin_name    The string used to version this plugin.
	 */
	protected $version;

  /**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 */
	public function __construct() {
    $this->version = '1.0.0';
		$this->plugin_name = 'woocommerce-gateway-business-on-account';
		$this->load_dependencies();
		$this->set_locale();
	}

  /**
   * Initialize the plugin
   *
   * @return void
   */
  public function run() {
    if ( !class_exists( 'WooCommerce' ) ) {
     return new WP_Error( 'error', __( "Woocommerce not active, can't add payement method", "woocommerce-gateway-business-on-account" ) );
    }
    add_action( 'wp_enqueue_scripts', [$this, 'enqueue_scripts'] );
    add_action( 'woocommerce_available_payment_gateways', [$this, 'maybe_remove_on_account_payment_method'] );
    add_action( 'woocommerce_checkout_update_order_review', [$this, 'set_session_company']);
    add_action( 'woocommerce_payment_gateways', [$this, 'add_gateway']);
    add_action( 'woocommerce_review_order_before_payment', [$this, 'add_invoice_notice']);
  }

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * @access   private
	 */
	private function load_dependencies() {
    require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-wc-gateway-business-on-account.php';
  }

	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * @access   private
	 */
	private function set_locale() {
		add_action( 'plugins_loaded', function(){
      load_plugin_textdomain( 'woocommerce-gateway-business-on-account', false,  basename( dirname( __DIR__ ) ) . '/languages' );
    });
	}

  /**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @return    string    The name of the plugin.
	 */
	public function get_plugin_name() {
		return $this->plugin_name;
	}

  public function add_invoice_notice() {
    echo '<div style="display:none" class="woocommerce-info js-invoice-notice">' . __('Betalen per factuur? Dit geldt alleen voor bedrijven. Vul hiervoor uw bedrijfsnaam in.', 'woocommerce-gateway-business-on-account') . '</div>';
  }

  /**
   * Add custom payment gateway
   *
   * @param [array] $methods payment methods
   * @return void
   */
  public function add_gateway($methods) {
    // Remove BACS payment to prevent duplication
		if(($key = array_search('WC_Gateway_BACS', $methods)) !== false) {
			unset($methods[$key]);
		}
    $methods[] = new WC_Gateway_On_Account;
    return $methods;
  }

  /**
   * Enqueue css and js needed for this plugin
   *
   * @return void
   */
  public function enqueue_scripts() {
    // Register payment loading scripts on checkout page
    wp_register_script( 'conditional-on-account', plugin_dir_url( dirname( __FILE__ ) ) . 'assets/conditional-on-account.js', ['woocommerce', 'wc-country-select'], $this->version, TRUE );
    wp_enqueue_script( 'conditional-on-account' );
  }

  /**
   * Remove the on-account payment method if the company field isn't filled in
   *
   * @param [array] $paymentMethods
   * @return void
   *
   */
  public function maybe_remove_on_account_payment_method($paymentMethods) {
    if( !is_admin() ) {
      $companyName = $this->get_session_company(WC()->session->get('customer'));
      if (!$companyName || empty($companyName)) {
        unset($paymentMethods['on-account']);
      }
    }
    return $paymentMethods;
  }

  /**
   * Set the updated company to the session
   *
   * We need this so we can later determine if our payment methods should be loaded
   *
   * @param [string] $post url-encoded string, got from the update_checkout Ajax hook
   * @return void
   */
  public function set_session_company($post) {
    $data = [];
    parse_str($post, $data);
    WC()->customer->set_props([
      'billing_company' => isset( $data['billing_company'] ) ? wp_unslash( $data['billing_company'] ) : null
    ]);
  }

  /**
   * Get the billing company from the session date
   *
   * @param boolean|array $sessionData
   * @return void
   */
  protected function get_session_company( $sessionData = false ) {
    return $sessionData && $sessionData['company'] != null ?  $sessionData['company'] : false;
  }
}