<?php
/**
 * The payment Gateway
 *
 * Extends to BACS gatewat, only overwrite difference
 *
 * @package    Woocommerce_Gateway_Business_On_Account
 * @subpackage Woocommerce_Gateway_Business_On_Account/includes
 * @author     Robbert de Kuiper <mail@robbertdekuiper.com>
 * @since             1.0.0
*/
class WC_Gateway_On_Account extends WC_Gateway_BACS {

  public function __construct() {

    $this->id                 = 'on-account';
    $this->icon               = apply_filters( 'woocommerce_bacs_icon', '' );
    $this->has_fields         = false;
    $this->method_title       = __( 'On account', 'woocommerce-gateway-business-on-account' );
    $this->method_description = __( 'Allows company payments on account', 'woocommerce-gateway-business-on-account' );
    $this->max_order_amount_sent_before_pay = 300;

    // Load the settings.
    $this->init_form_fields();
    $this->init_settings();

    // Define user set variables
    $this->title        = $this->get_option( 'title' );
    $this->description  = $this->get_option( 'description' );
    $this->instructions = $this->get_option( 'instructions' );

    // BACS account fields shown on the thanks page and in emails
    $this->account_details = get_option( 'woocommerce_bacs_accounts',
        array(
            array(
                'account_name'   => $this->get_option( 'account_name' ),
                'account_number' => $this->get_option( 'account_number' ),
                'sort_code'      => $this->get_option( 'sort_code' ),
                'bank_name'      => $this->get_option( 'bank_name' ),
                'iban'           => $this->get_option( 'iban' ),
                'bic'            => $this->get_option( 'bic' ),
            ),
        )
    );

    // Actions
    add_action( 'woocommerce_update_options_payment_gateways_' . $this->id, [$this, 'process_admin_options']);
    add_action( 'woocommerce_update_options_payment_gateways_' . $this->id, [$this, 'save_account_details']);
    add_action( 'woocommerce_thankyou_on-account', [$this, 'thankyou_page'] );
    add_action( 'woocommerce_email_order_details', [$this, 'add_email_payment_notice'], 9, 2);
    add_action( 'wpo_wcpdf_after_order_details', [$this, 'pdf_instructions'], 10, 2);
  }

  /**
	 * Initialise Gateway Settings Form Fields.
	 */
	public function init_form_fields() {
		$this->form_fields = array(
			'enabled'         => array(
				'title'   => __( 'Enable/Disable', 'woocommerce' ),
				'type'    => 'checkbox',
				'label'   => __( 'Enable bank transfer', 'woocommerce' ),
				'default' => 'no',
			),
			'title'           => array(
				'title'       => __( 'Title', 'woocommerce' ),
				'type'        => 'text',
				'description' => __( 'This controls the title which the user sees during checkout.', 'woocommerce' ),
				'default'     => __( 'Direct bank transfer', 'woocommerce' ),
				'desc_tip'    => true,
			),
			'description'     => array(
				'title'       => __( 'Description', 'woocommerce' ),
				'type'        => 'textarea',
				'description' => __( 'Payment method description that the customer will see on your checkout.', 'woocommerce' ),
				'default'     => __( 'Make your payment directly into our bank account. Please use your Order ID as the payment reference. Your order will not be shipped until the funds have cleared in our account.', 'woocommerce' ),
				'desc_tip'    => true,
			),
			'instructions'    => array(
				'title'       => __( 'Instructions', 'woocommerce' ),
				'type'        => 'textarea',
				'description' => __( 'Instructions that will be added to the thank you page, emails and PDF. You can use the placeholder {order-number} for the order number.', 'woocommerce-gateway-business-on-account' ),
				'default'     => '',
				'desc_tip'    => true,
			),
		);
	}

  /**
	 * Output for the order received page.
	 *
	 * @param int $order_id Order ID.
	 */
	public function thankyou_page( $order_id ) {
    echo wpautop($this->get_instructions( wc_get_order($order_id) ));
	}

  /**
	 * Get the instructions string
   * Conversts the variable '{order-number}' to the order number
	 *
	 * @param int $order_id Order ID.
	 */
	protected function get_instructions( $order = false ) {
		if ( $order && $this->instructions ) {
			$instructions = wp_kses_post( wptexturize( $this->instructions ) );
      return str_replace('{order-number}',  '<strong>' . $order->get_order_number() . '</strong>', $instructions);
		}
	}

  /**
   * Process the payment and return the result
   *
   * @access public
   * @param int $order_id
   * @return array
   */
  function process_payment( $order_id ) {
    $order = wc_get_order( $order_id );
    // Mark as processing, sent orders before payment
    $this->set_order_status( $order );
    // Reduce stock levels
    wc_reduce_stock_levels( $order_id );
    // Remove cart
    WC()->cart->empty_cart();
    // Return thankyou redirect
    return array(
      'result'    => 'success',
      'redirect'  => $this->get_return_url( $order ),
    );
  }

  /**
   * Set the order status based on the total amount of the order
   *
   * @param object $order
   * @return void
   */
  private function set_order_status( $order ) {
    if ( $order->get_total() > $this->max_order_amount_sent_before_pay ) {
       $order->update_status( 'deliver-now', __( 'Set status on account order with total above € ' . $this->max_order_amount_sent_before_pay . ' to deliver now.' , 'woocommerce-gateway-business-on-account' ) );
    } else {
      $order->update_status( 'processing', __( 'Processing on account order', 'woocommerce-gateway-business-on-account' ) );
    }
  }

  /**
   * Add Payment instruction to order email
   *
   * @param object $order
   */
  public function add_email_payment_notice($order, $sent_to_admin = false) {
    if (!$sent_to_admin && 'on-account' === $order->get_payment_method() && $this->instructions ) {
      echo "<p><strong>". __('Payment method:', 'woocommerce') . "</strong><br>";
	    echo $this->get_instructions($order) . '</p>' . PHP_EOL;
    }
  }

  /**
    * Add payment instructions to the Package and slips PDFs.
    *
    * @param string $type
    * @param object $order
    */
  public function pdf_instructions( $type, $order ) {
    if( 'invoice' ===  $type ) {
      $this->add_email_payment_notice($order);
    }
  }
}